package tn.jsfTraining.utility;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import tn.jsfTraining.entities.User;
import tn.jsfTraining.services.UserService;



/**
 * Session Bean implementation class AppUtilities
 */
@Singleton
@LocalBean
@Startup
public class AppUtilities {
	@EJB
	private UserService us;


	/**
	 * Default constructor.
	 */
	public AppUtilities() {
	}

	@PostConstruct
	public void initDb() {
		us.registerUser(new User("khreibi.mohamed@gmail.com","mohamed","admin","ADMIN"));
		us.registerUser(new User("maissen.ayed@gmail.com","maissen","admin","ADMIN"));
	}
}
