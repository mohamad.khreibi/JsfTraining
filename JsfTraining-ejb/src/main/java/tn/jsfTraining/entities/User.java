package tn.jsfTraining.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User implements Serializable {
	

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id;
	
	private String email;
	private String userName;
	private String password;
	private String roles;
	
	public User() {
		super();
	}
	
	public String getRoles()
	{return roles;}
	
	public void setRoles(String roles){
		this.roles=roles;
	}
	public void addRole(String role){
		this.roles+=role.toUpperCase()+",";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public User(String email, String userName, String password, String roles) {
		super();
		this.email = email;
		this.userName = userName;
		this.password = password;
		this.roles = roles;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean hasRole(String role){
	
		String[] rolesArray=roles.split(",");
		int i=0;
		boolean ok=false;
		do
		{
			ok=role.equals(rolesArray[i]);
			i++;
		}
		while(!ok || i<rolesArray.length);
		return ok;
	}
	

	

}
