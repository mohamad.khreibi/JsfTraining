package tn.jsfTraining.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.jsfTraining.entities.User;
import tn.jsfTraining.utility.EncrypterClass;

@Stateless
@LocalBean
public class UserService extends AbstractFacade<User> {

	public UserService() {
		super(User.class);
	}
	
	
    @PersistenceContext
    private EntityManager em;


	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	
	
	public void registerUser(User u){
		
		   try {
			u.setPassword(EncrypterClass.Password.getSaltedPassword(u.getPassword()));
			create(u);
		   	} catch (Exception e) {
			e.printStackTrace();
			}	
		}
	
	
	public User findByUserName(String username){	
		User result=null;
		try
		{
		 result = (User)em.createQuery("select u from User u where  u.userName = :username").setParameter("username", username).getSingleResult(); 
		}
		catch(Exception e){
			System.out.println("findByUsername");
		}
			
		 return result;
	}
	
	
	
	private boolean verifyUser(String username,String pswd){
		
		boolean ok=false;
		User u=null;
		try {
		    u= findByUserName(username);
			
			ok= EncrypterClass.Password.match(pswd, u.getPassword());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ok;
	}
	
	
	public User login(String userName,String pswd)
	{
		User u=null;
	    boolean ok= verifyUser(userName, pswd);
	    if(ok)
	    {u=findByUserName(userName);}
	    
	    return u;
	}
	
	public void updateUserNoPassword(User u){
		User tempUser=find(u.getId());
	    u.setPassword(tempUser.getPassword());
	    update(u);
	}






}
