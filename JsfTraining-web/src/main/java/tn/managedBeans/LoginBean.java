package tn.managedBeans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.constraints.Size;

import tn.jsfTraining.entities.User;
import tn.jsfTraining.services.UserService;

@ManagedBean(name="loginBean",eager=true)
public class LoginBean {
	

	@EJB 
	private UserService us;

	
	
    @Size(min=4, max=10)
    private String username;
 
    @Size(min=4, max=10)
    private String password;
 
 
    public void setUsername(String name) {
        this.username = name;
    }
 
    public String getUsername() {
        return username;
    }
 
 
    public String getPassword() {
        return password;
    }
 
 
    public void setPassword(String password) {
        this.password = password;
    }
 
    public void login() {
    	User u=null;
    	u=us.login(username, password);
    	System.out.println(username);
        if (u!=null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Congratulations! You've successfully logged in.");
            System.out.println("salut");
            FacesContext.getCurrentInstance().addMessage("loginForm:password", msg);
 
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "That's the wrong password. Hint: BootsFaces rocks!");
            FacesContext.getCurrentInstance().addMessage("loginForm:password", msg);
        }
    }
 
    public void forgotPassword() {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Default user name: BootsFaces");
        FacesContext.getCurrentInstance().addMessage("loginForm:username", msg);
        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Default password: rocks!");
        FacesContext.getCurrentInstance().addMessage("loginForm:password", msg);
    }
}